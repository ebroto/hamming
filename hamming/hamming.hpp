/** @file */
#ifndef HAMMING_HPP_
#define HAMMING_HPP_

#include <cstdint>
#include <cstring>

/** \mainpage Hamming distance library
 * This all-header library exposes two free functions called hamming_distance. The first
 * overload calculates the hamming distance between two integers. The second
 * overload calculates the hamming distance between two binary blobs of a given
 * size.
 *
 * Look at the "Files" section for more detailed documentation.
 */

// Check for GCC compiler. If we are using GCC, we will use
// a builtin function to calculate the number of bits set to 1
// with one machine instruction.
//
#if defined(__GNUG__) && !defined(__clang__)
#define IS_GCC
#endif

// Checks if the type passed as parameter is unsigned. As we are dealing
// with binary blobs, integers used must be unsigned.
//
#define UNSIGNED_CHECK(Type) \
	static_assert(std::is_unsigned<Type>::value, "Parameter type must be unsigned");

namespace util {
using byte = unsigned char;

// Namespace detail is used for internal functions.
//
namespace detail {
#ifdef IS_GCC
	/**
	 * Calculates the number of bits set to 1. Calls one machine instruction.
	 * @param value Input value.
	 * @return Number of bits set to 1
	 */
	template <typename IntegerType>
	IntegerType popcount(IntegerType value) {
		return __builtin_popcountll(value);
	}

	/**
	 * Calculates the number of bits set to 1. Calls one machine instruction.
	 * Specialization of unsigned long values.
	 * @param value Input value.
	 * @return Number of bits set to 1
	 */
	template <>
	unsigned long popcount(unsigned long value) {
		return __builtin_popcountl(value);
	}

	/**
	 * Calculates the number of bits set to 1. Calls one machine instruction.
	 * Specialization of unsigned int values.
	 * @param value Input value.
	 * @return Number of bits set to 1
	 */
	template <>
	unsigned int popcount(unsigned int value) {
		return __builtin_popcount(value);
	}
#else
	/**
	 * Calculates the number of bits set to 1. Unoptimized version.
	 * @param value Input value.
	 * @return Number of bits set to 1
	 */
	template <typename IntegerType>
	IntegerType popcount(IntegerType value) {
		IntegerType distance {0};

		while (value != 0)
		{
			distance++;
			value &= value - 1;
		}

		return distance;
	}
#endif

	/**
	 * Consumes size bytes from a binary blob.
	 * @param blob Binary blob.
	 * @param size Size of the blob.
	 * @return Integer with the consumed value.
	 */
	template <typename IntegerType>
	IntegerType consume(byte*& blob, std::size_t size) {
		// The objective of consume is to fill an integer that can be passed to popcount functions.
		// We test if we can read a full integer of the specified size. If we have enough data,
		// we fill the integer. If there's not enough data, we pad the rest of the integer with zeroes.
		//
		// NOTE: depending on the architecture, we are not respecting byte ordering but that's not
		// relevant since popcount will compare the values bit to bit and the resulting difference will
		// be the same.
		//
		const std::size_t copy_sz {size >= sizeof(IntegerType)? sizeof(IntegerType) : size};
		const std::size_t padd_sz {copy_sz < sizeof(IntegerType)? sizeof(IntegerType) - copy_sz : 0};
		IntegerType result;

		if (copy_sz) {
			std::memcpy(&result, blob, copy_sz);
			std::advance(blob, copy_sz);
		}

		if (padd_sz) {
			std::memset(reinterpret_cast<byte*>(&result) + copy_sz, 0, padd_sz);
		}

		return result;
	}
}

/**
 * Calculate hamming distance between two integers.
 * @param lhs First integer.
 * @param rhs Second integer.
 */
template <typename IntegerType>
IntegerType hamming_distance(IntegerType lhs, IntegerType rhs) {
	UNSIGNED_CHECK(IntegerType);

	return detail::popcount(lhs ^ rhs);
}

/**
 * Calculate hamming distance between two binary blobs. It will consume the buffers in sizeof(IntegerType)
 * chunks and call hamming_distance(IntegerType, IntegerType) until there's no more information left on
 * the buffers. The default type for IntegerType is the fastest 64 bit unsigned integer available.
 * Changing this type could improve the performance for example in 32 bit architectures.
 * @param left_blob First binary blob.
 * @param right_blob Second binary blob.
 * @param size Size of the blobs.
 */
template <typename IntegerType = std::uint_fast64_t>
IntegerType hamming_distance(byte* left_blob, byte* right_blob, std::size_t size) {
	// This overload consumes the blobs by trying to retrieve chunks of sizeof(IntegerType) size.
	// It is done that way because calling xor and popcount on machine word sized variables is
	// presumably faster than calling them byte to byte.
	//
	// NOTE: the performance of the overall process could be improved by parallellizing for example
	// with a pool of threads (boost) or using c++17 parallel algorithm execution policies and
	// creating a functor whose operator() calculates xor and popcount for a word sized fragment
	// of the blob. It is left this way to keep the algorithm simple.
	//
	UNSIGNED_CHECK(IntegerType);

	IntegerType distance {0}, left {0}, right {0};
	int remaining {static_cast<int>(size)};

	while (remaining > 0) {
		left = detail::consume<IntegerType>(left_blob, remaining);
		right = detail::consume<IntegerType>(right_blob, remaining);
		distance += hamming_distance(left, right);
		remaining -= sizeof(IntegerType);
	}

	return distance;
}

/**
 * Helper to access the underlaying data of a container.
 * @param in STL Container.
 * @return Underlaying contents as a byte pointer.
 */
template <typename Container>
byte* get_bytes(const Container& in) {
	return reinterpret_cast<byte*>(const_cast<char*>(in.data()));
}
}

#endif
