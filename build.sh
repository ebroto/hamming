#!/bin/bash
#
echo "building..."
if [[ ! -d "build" ]]; then
	mkdir build
fi

if [[ ! -d "bin" ]]; then
	mkdir bin
fi

cd build && cmake .. && make

if [[ $? -eq 0 ]]; then
	echo "build ok"
	
	if [[ -f "hamming_tests" ]]; then
		echo "running tests"
		./hamming_tests
	fi
	
	if [[ -f "hamming" ]]; then
		cp hamming ../bin
		echo "Executable \"hamming\" copied to bin/"
	fi
else
	echo "build error"
fi
