#define CATCH_CONFIG_MAIN
#include "../catch/catch.hpp"
#include "../hamming/hamming.hpp"

using ull = unsigned long long;

ull test_strings(const std::string& left, const std::string& right) {
	return util::hamming_distance(util::get_bytes(left), util::get_bytes(right), left.size());
}

TEST_CASE("Hamming distances are computed", "[hamming]") {
	REQUIRE(test_strings("InputString1", "InputString2") == 2);
	REQUIRE(test_strings("I", "K") == 1);
	REQUIRE(util::hamming_distance(0xFFFFFFFFU, 0xFFFFFFFEU) == 1);
	REQUIRE(util::hamming_distance(0xBADC0FEEU, 0xFFFFFFFFU) == 12);
}
