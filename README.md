# README #

Simple single header library to calculate bit-wise hamming distance.

### Requirements ###

A modern compiler. Tested with gcc 6.1.1.

### Structure ###

* hamming/ => Library source files
* tests/ => Test-related files
* doc/ => Doxygen documentation
* catch/ => Test framework library
* bin/ => Binaries directory
* build/ => Out of source build directory

### Usage ###

Include hamming.hpp in your project and use the functions.

### How to build the command line utility ###

Run build.sh from the root directory of the repository. 

```
#!bash
bash$ git clone https://ebroto@bitbucket.org/ebroto/hamming.git hamming
bash$ cd hamming
bash$ ./build.sh
```

If everything goes well a simple command line utility will be generated under **bin/**. It takes
two strings passed as parameters. Examples:


```
#!bash

bash$ bin/hamming "test string 1" "test string 2"
Hamming distance: 2
bash$ bin/hamming "lorem ipsum" "dolor sit amet"
Error: sizes differ: 11 != 14
```


### How to run the tests ###

Tests are run every time a build is completed. If you want to execute them manually run **tests/hamming_tests** from the root directory of the repository.