#include <iostream>
#include "hamming/hamming.hpp"

// Command line utility to calculate the hamming distance between two strings
// provided as input parameters.
//
int main(int argc, char* argv[]) {
	// Check correct usage
	//
	const std::string program_name {argv[0]};

	if (argc != 3) {
		std::cout
			<< "usage:\n"
			<< '\t' << program_name << " input_string1 input_string2\n\n"
			<< "examples:\n"
			<< '\t' << program_name << " example1 example2\n"
			<< '\t' << program_name << " \"example with spaces\" example2\n" << std::endl;
		return 1;
	}

	// Calculate the hamming distance between the contents of the two strings
	//
	std::string input1 {argv[1]}, input2 {argv[2]};

	if (input1.size() == input2.size()) {
		const auto distance = util::hamming_distance(
				util::get_bytes(input1),
				util::get_bytes(input2),
				input1.size());

		std::cout << "Hamming distance: " << distance << std::endl;
	} else {
		std::cerr << "Error: sizes differ: " << input1.size() << " != " << input2.size() << std::endl;
	}

	return 0;
}
